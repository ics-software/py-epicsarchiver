# Command Line Documentation

```{eval-rst}
.. click:: epicsarchiver.command:cli
    :prog: epicsarchiver
    :nested: full
```
