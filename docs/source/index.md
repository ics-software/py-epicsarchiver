
```{include} ../../README.md
```


```{toctree}
:hidden:
:caption: Reference

installation
License <license>
```

```{toctree}
:hidden:
:caption: Terminal Interface

usage
notebooks/CmdLine
```

```{toctree}
:hidden:
:caption: Python Library

notebooks/Library
```

```{eval-rst}
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```

[license]: license
[command-line reference]: usage
[reference]: autoapi
