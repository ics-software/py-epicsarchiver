"""Configuration file for the Sphinx documentation builder.

This file only contains a selection of the most common options. For a full
list see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""
from importlib.metadata import version as get_version

# -- Project information -----------------------------------------------------

project = "epicsarchiver"
copyright = "2024, European Spallation Source ERIC"  # noqa: A001
authors = ["Benjamin Bertrand", "Sky Brewer"]
release = get_version("py-epicsarchiver")
version = ".".join(release.split(".")[0:2])


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "myst_nb",
    "sphinx_click",
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.viewcode",
    "autoapi.extension",
    "sphinx_copybutton",
    "sphinx.ext.napoleon",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


autoapi_dirs = ["../../epicsarchiver"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path: list[str] = []

# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
}

# Enable special syntax for admonitions (:::{directive})
myst_admonition_enable = True

# Enable definition lists (Term\n: Definition)
myst_deflist_enable = True

# Allow colon fencing of directives
myst_enable_extensions = [
    "colon_fence",
]

# Don't execute notebooks in documentation building
nb_execution_mode = "off"

# Ignore highlighting ansi in notebooks
supress_warnings = ["misc.highlighting_failure"]
