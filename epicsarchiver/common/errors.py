"""Module of custom errors."""


class ArchiverResponseError(Exception):
    """Problem with the response returned by the Archiver Appliance."""
