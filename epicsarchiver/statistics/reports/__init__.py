"""Reports for the statistics module."""

REPORT_CSV_HEADINGS = [
    "IOC Name",
    "IOC hostname",
    "PV name",
    "Statistic",
    "Statistic Note",
]
